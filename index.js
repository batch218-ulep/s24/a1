// console.log("Mabuhay!");

const getCube = Math.pow(2, 3);
console.log(`The cube of 2 is ${getCube}`)

const address = ["258", "Washington Ave MW", "California", "90011"];
const [ houseNumber, street, state, zipCode ] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	species: "crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const {name, species, weight, measurement} = animal;
console.log(`${name} was a saltwater ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


const arrNums = [1, 2, 3, 4, 5];
const displayNums = arrNums.forEach((num) => {
	console.log(`${num}`);
});

const reduceNumber = arrNums.reduce((x, y) => {
	return x + y;
})
console.log(reduceNumber);

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);
